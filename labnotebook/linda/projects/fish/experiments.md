### 02/07/17
* collected corals [images](https://figshare.com/s/5bf9410b2e85376f1e1a)
* extracted DNA from coral
  * [Protocol](https://github.com/lindablackall/bioinf/blob/master/labnotebook/protocols/extract_dna.md) 
  * Notes
    * In step 2, I only centrifuged at 1000rpm because high speed centrifuge wasn't working 
* [results](https://figshare.com/s/fd0f1c247965f7401054)

### 01/07/17
* DNA extractions Wayne's method
* place ....
* test edit

### 30/6/17
* met about choosing probes
* ordered probes
