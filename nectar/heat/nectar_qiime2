# http://docs.openstack.org/developer/heat/template_guide/hot_spec.html#heat-template-version
heat_template_version: 2014-10-16


description: NeCTAR Sample Template - A very simple one that just fires up an instance


parameters:

  key_name:
    type: string
    description: Name of an existing KeyPair to enable SSH access to the instances
    # can use the provided custom constraint to check that the keypair does exist
    default: Nectar_Key
    constraints:
      - custom_constraint: nova.keypair
        description: Must be an existing keypair

  instance_type:
    type: string
    description: The NeCTAR flavour the webserver is to run on
    default: m2.medium
    constraints:
      - allowed_values: [m2.small, m2.medium, m2.large, m1.xlarge, m1.xxlarge, mel.bigmem2.medium]
        description:
          Must be a valid NeCTAR flavour, limited to the smaller ones available

  availability_zone:
    type: string
    label: Availability Zone
    description: Physical location of the server. 
    default: melbourne-qh2-uom
    constraints:
      - allowed_values: [ monash, melbourne-qh2-uom, QRIScloud, NCI, intersect, pawsey, sa, tasmania ]
        description: Value must be one of monash, melbourne, QRIScloud, NCI, intersect, pawsey, sa, tasmania.

  image_id:
    type: string
    description: ID of the image to use for the instance to be created
    default: 99d9449a-084f-4901-8bd8-c04aebd589ca


resources:

  security_group:
    type: AWS::EC2::SecurityGroup
    properties:
      GroupDescription: Web server access rules.
      SecurityGroupIngress:
        - {IpProtocol: icmp, FromPort: '-1', ToPort: '-1', CidrIp : 0.0.0.0/0}
        - {IpProtocol: tcp, FromPort: '22', ToPort: '22', CidrIp: 0.0.0.0/0}
        - {IpProtocol: tcp, FromPort: '80', ToPort: '80', CidrIp: 0.0.0.0/0}
        - {IpProtocol: tcp, FromPort: '443', ToPort: '443', CidrIp: 0.0.0.0/0}
        - {IpProtocol: tcp, FromPort: '20', ToPort: '21', CidrIp: 0.0.0.0/0}


  server_init:
    #http://docs.openstack.org/developer/heat/template_guide/openstack.html#OS::Heat::SoftwareConfig
    type: OS::Heat::SoftwareConfig
    properties:
      group: ungrouped
      config: |
        #!/bin/sh
        mkdir miniconda
        cd miniconda
        sudo apt install curl
        curl -o mc.sh https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
        chmod +x mc.sh
        bash mc.sh -b
        conda update conda
        conda install wget
        wget https://data.qiime2.org/distro/core/qiime2-2019.7-py36-linux-conda.yml
        conda env create -n qiime2-2019.7 --file qiime2-2019.7-py36-linux-conda.yml
        rm qiime2-2019.7-py36-linux-conda.yml

  basic_server:
    # http://docs.openstack.org/developer/heat/template_guide/openstack.html#OS::Nova::Server
    type: OS::Nova::Server
    properties:
      key_name: { get_param: key_name }
      security_groups:
        - { get_resource: security_group }
      image: { get_param: image_id }
      availability_zone: {get_param: availability_zone}
      flavor: { get_param: instance_type }
      networks:
        - allocate_network: auto
      user_data_format: RAW
      user_data:
        get_resource: server_init
