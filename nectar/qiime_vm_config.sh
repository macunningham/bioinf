#!/bin/sh
cd
mkdir miniconda
cd miniconda
apt install curl
curl -o mc.sh https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x mc.sh
bash mc.sh -b
export PATH="$HOME/miniconda3/bin:$PATH"
conda create -n qiime1 python=2.7 qiime matplotlib=1.4.3 mock nose -c bioconda
