#!/usr/bin/env nextflow

params.input = "$baseDir/MiSeq_SOP/*.fastq"
fqs = Channel.fromPath(params.input)
template = file("$baseDir/template/R_list_files.sh")

process R_hello {
    output:
    stdout result

    input:
    file fq from fqs

    script:
    template "${template}"
}

result.subscribe {
    println it.trim()
}
