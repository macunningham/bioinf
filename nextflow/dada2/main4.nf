#!/usr/bin/env nextflow

params.input = "$baseDir/MiSeq_SOP"
st = Channel.fromPath(params.input).filter("seqtab.nochim")

params.outDir="$baseDir/output"

process Dada2 {

    publishDir params.outDir, mode: 'copy'

    output:
    stdout result

    input:
    file st

    script:
    """
    #!/usr/bin/env Rscript

    print("loading dada2 library")
    library(dada2)

    path <- "${params.input}"
    print("path")
    print(path)

    import(system.file("tests", "st"))

    rdppath <- file.path(path, "rdp_train_set_16.fa.gz")

    seqtab.nochim <- numeric(0)
    taxa <- assignTaxonomy(seqtab.nochim, rdppath, multithread=TRUE)

    """
}

result.subscribe {
    println it.trim()
}
