#!/usr/bin/env nextflow

params.input = "$baseDir/input"
params.outDir= "$baseDir/output"

test = Channel.fromPath( '/home/ubuntu/nf/input/*.gz' )

fqs = Channel.fromPath(params.input)
rdp = Channel.fromPath(params.input).filter('rdp_training_set_16.fa.gz')

process Dada2 {

    publishDir params.outDir, mode: 'copy'

    input:
    file fq from fqs.first()
    file test

    output:
    stdout result
    file "plotQualityProfile_forward.png" into Ch1
    file "plotQualityProfile_reverse.png" into Ch2
    file "plotErrors_forward.png" into Ch3
    file "seqtab.out" into Ch4
    file "taxa.txt" into Ch5

    script:
    """
    #!/usr/bin/env Rscript

    print("loading dada2 library")
    library(dada2)

    path <- "${params.input}"
    print("path")
    print(path)
    fnFs <- sort(list.files(path, pattern="_R1_001.fastq"))
    fnRs <- sort(list.files(path, pattern="_R2_001.fastq"))

    # Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq
    sample.names <- sapply(strsplit(fnFs, "_"), `[`, 1)
    # Specify the full path to the fnFs and fnRs
    fnFs <- file.path(path, fnFs)
    fnRs <- file.path(path, fnRs)

    # Prepare read quality profiles
    png('plotQualityProfile_forward.png')
    plotQualityProfile(fnFs[1:2])
    dev.off()
    png('plotQualityProfile_reverse.png')
    plotQualityProfile(fnRs[1:2])
    dev.off()

    # Perform filtering and trimming
    filt_path <- file.path(path, "filtered") # Place filtered files in filtered/ subdirectory
    filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt.fastq.gz"))
    filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt.fastq.gz"))
    out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, truncLen=c(240,160),
              maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=TRUE) # On Windows set multithread=FALSE

    # Learn error rates
    errF <- learnErrors(filtFs, multithread=TRUE)
    errR <- learnErrors(filtRs, multithread=TRUE)

    # Print error forward read error plot
    png('plotErrors_forward.png')
    plotErrors(errF, nominalQ=TRUE)
    dev.off()

    # Dereplicate sequences
    derepFs <- derepFastq(filtFs, verbose=TRUE)
    derepRs <- derepFastq(filtRs, verbose=TRUE)

    # Name the derep-class objects by the sample names
    names(derepFs) <- sample.names
    names(derepRs) <- sample.names

    # Infer sequence variants
    dadaFs <- dada(derepFs, err=errF, multithread=TRUE)
    dadaRs <- dada(derepRs, err=errR, multithread=TRUE)
    mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, verbose=TRUE)

    # Inspect the merger data.frame from the first sample
    head(mergers[[1]])

    # Make Sequence Table
    seqtab <- makeSequenceTable(mergers)
    dim(seqtab)
    table(nchar(getSequences(seqtab)))

    # Remove Chimeric Sequences
    seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
    dim(seqtab.nochim)
    sum(seqtab.nochim)/sum(seqtab)
    # saveRDS(seqtab.nochim, "seqtab.nochim.rds")
    save(seqtab.nochim, file = "seqtab.out", ascii = TRUE)


    # Report read counts by pipeline stage
    getN <- function(x) sum(getUniques(x))
    track <- cbind(out, sapply(dadaFs, getN), sapply(mergers, getN), rowSums(seqtab), rowSums(seqtab.nochim))
    colnames(track) <- c("input", "filtered", "denoised", "merged", "tabled", "nonchim")
    rownames(track) <- sample.names
    head(track)

    # Sequence variant taxonomic classification

    taxa <- assignTaxonomy(seqtab.nochim, "${test}", multithread=TRUE)
    unname(head(taxa))
    un_taxa <- unname(taxa)
    head(un_taxa)
    save(un_taxa, file = "taxa.txt", ascii = TRUE)
    """
}

result.subscribe {
    println it.trim()
}
