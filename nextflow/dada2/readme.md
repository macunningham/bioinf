to run full pipeline need following directory structure (more than one pair of fwd / rev fastq reads seems to be necessary)..

nextflow.config
main5.nf
input /
  F3D141_S207_L001_R1_001.fastq
  F3D141_S207_L001_R2_001.fastq
  F3D142_S208_L001_R1_001.fastq
  F3D142_S208_L001_R2_001.fastq
  F3D143_S209_L001_R1_001.fastq
  F3D143_S209_L001_R2_001.fastq
  rdp_train_set_16.fa.gz

run following command from directory where main5.nf file is located
sudo nextflow run main5.nf -with-docker mitchac/dada2
