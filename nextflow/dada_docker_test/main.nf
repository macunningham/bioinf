#!/usr/bin/env nextflow

process R_hello {
    output:
    stdout result

    script:
    """
    #!/usr/bin/env Rscript
    print("Hello World!")
    """
}

result.subscribe {
    println it.trim()
}
