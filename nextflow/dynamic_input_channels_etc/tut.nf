#!/usr/bin/env nextflow

/*
 * passing tests
 * - conditional execute process 
 * - specify process input channel at runtime 
 * - channel from parameter 
 * - variables from params.yaml file run with file content eg {"foo":'fooText', "bar":'barText'}  
 * run with 
 * sudo nextflow run tut.nf -params-file params.yaml
 */

params.str = 'Hello world!'
params.splitRun  = 'y'
params.upperRun  = 'y'
splitLettersChout = Channel.create()
splitLetters2Chout = Channel.create()
convertToUpperChin = splitLettersChout

process splitLetters {
    when:
    params.splitRun == 'y'
    
    output:
    file 'chunk_*' into splitLettersChout  mode flatten

    """
    printf '${params.foo}' | split -b 6 - chunk_
    """
}

process splitLetters2 {
    when:
    params.splitRun == 'y'

    output:
    file 'chunk_*' into splitLetters2Chout  mode flatten

    """
    printf '${params.str}' | split -b 2 - chunk_
    """
}

process convertToUpper {
    
    when:
    params.upperRun == 'y'

    input:
    file x from convertToUpperChin

    output:
    stdout result

    """
    cat $x | tr '[a-z]' '[A-Z]'
    """
}

result.subscribe {
    println it.trim()
}
