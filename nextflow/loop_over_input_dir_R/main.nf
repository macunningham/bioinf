#!/usr/bin/env nextflow

params.input = "$baseDir/MiSeq_SOP/*.fastq"
fqs = Channel.fromPath(params.input)

process R_hello {
    output:
    stdout result

    input:
    file fq from fqs

    """

    #!/usr/bin/env Rscript
    print("${fq.baseName}")

    """
}

result.subscribe {
    println it.trim()
}
