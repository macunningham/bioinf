#!/usr/bin/env nextflow

params.input = "data/MiSeq_SOP/*.fastq"
fqs = Channel.fromPath(params.input)

process R_hello {
    output:
    stdout result



    script:
    """
    #!/usr/bin/env Rscript
    print(getwd())
    path <- "${params.input}" # CHANGE ME to the directory containing the fastq files after unzipping.
    print(path)
    #fnFs <- sort(list.files(path, pattern="_R1_001.fastq"))
    fnFs <- list.files(path)
    print(length(fnFs))
    """
}

result.subscribe {
    println it.trim()
}
