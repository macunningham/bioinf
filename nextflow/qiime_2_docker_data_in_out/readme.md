procedure for verifying data input and output from qiime 2 docker. 
commands sourced from following link..
https://docs.qiime2.org/2017.6/tutorials/moving-pictures/


mkdir emp-single-end-sequences
wget -O "emp-single-end-sequences/barcodes.fastq.gz" "https://data.qiime2.org/2017.6/tutorials/moving-pictures/emp-single-end-sequences/barcodes.fastq.gz"
echo "hello"
wget -O "emp-single-end-sequences/sequences.fastq.gz" "https://data.qiime2.org/2017.6/tutorials/moving-pictures/emp-single-end-sequences/sequences.fastq.gz"
echo "hello2"
qiime tools import \
  --type EMPSingleEndSequences \
  --input-path emp-single-end-sequences \
  --output-path emp-single-end-sequences.qza
