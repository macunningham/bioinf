#!/usr/bin/env nextflow

params.loc="$baseDir/demux.qza"
qzafile= file(params.loc)

process qiime_hello {

    input:
    file qzafile

    output:
    stdout result

    """
    qiime demux summarize \
  --i-data ${qzafile} \
  --o-visualization demux.qzv
    """
}

result.subscribe {
    println it.trim()
}
