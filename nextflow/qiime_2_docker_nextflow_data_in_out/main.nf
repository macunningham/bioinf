#!/usr/bin/env nextflow

params.loc="$baseDir/demux.qza"
params.outDir="$baseDir"
qzafile= file(params.loc)

process qiime_hello {

    publishDir params.outDir, mode: 'copy'

    input:
    file qzafile

    output:
    file "demux.qzv" into testChannel
    """
    qiime demux summarize \
  --i-data ${qzafile} \
  --o-visualization demux.qzv
    """
}
