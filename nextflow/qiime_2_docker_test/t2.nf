#!/usr/bin/env nextflow

process qiime_hello {

    output:
    stdout result

    """
    #echo $PWD
    qiime info	 
    """
}

result.subscribe {
    println it.trim()
}
