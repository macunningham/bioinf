#!/usr/bin/env nextflow

params.input = "$baseDir/MiSeq_SOP"
fqs = Channel.fromPath(params.input)

params.outDir="$baseDir/output"

process R_hello {

    publishDir params.outDir, mode: 'copy'

    output:
    stdout result
    file "plotQualityProfile_forward.png" into Ch1
    file "plotQualityProfile_reverse.png" into Ch2
    file "plotErrors_forward" into Ch3

    input:
    file fq from fqs.first()

    script:
    """
    #!/usr/bin/env Rscript
    library(dada2)
    path <- "${params.input}"
    fnFs <- sort(list.files(path, pattern="_R1_001.fastq"))
    fnRs <- sort(list.files(path, pattern="_R2_001.fastq"))
    # Extract sample names, assuming filenames have format: SAMPLENAME_XXX.fastq
    sample.names <- sapply(strsplit(fnFs, "_"), `[`, 1)
    # Specify the full path to the fnFs and fnRs
    fnFs <- file.path(path, fnFs)
    fnRs <- file.path(path, fnRs)
    png('plotQualityProfile_forward.png')
    plotQualityProfile(fnFs[1:2])
    dev.off()
    png('plotQualityProfile_reverse.png')
    plotQualityProfile(fnRs[1:2])
    dev.off()
    filt_path <- file.path(path, "filtered") # Place filtered files in filtered/ subdirectory
    filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt.fastq.gz"))
    filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt.fastq.gz"))
    out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, truncLen=c(240,160),
              maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=TRUE) # On Windows set multithread=FALSE
    errF <- learnErrors(filtFs, multithread=TRUE)
    errR <- learnErrors(filtRs, multithread=TRUE)

    png('plotErrors_forward')
    plotErrors(errF, nominalQ=TRUE)
    dev.off()
    derepFs <- derepFastq(filtFs, verbose=TRUE)
    derepRs <- derepFastq(filtRs, verbose=TRUE)
    # Name the derep-class objects by the sample names
    names(derepFs) <- sample.names
    names(derepRs) <- sample.names
    dadaFs <- dada(derepFs, err=errF, multithread=TRUE)
    dadaRs <- dada(derepRs, err=errR, multithread=TRUE)
    mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, verbose=TRUE)
    # Inspect the merger data.frame from the first sample
    head(mergers[[1]])
    seqtab <- makeSequenceTable(mergers)
    dim(seqtab)
    table(nchar(getSequences(seqtab)))
    seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
    dim(seqtab.nochim)
    sum(seqtab.nochim)/sum(seqtab)
    getN <- function(x) sum(getUniques(x))
    track <- cbind(out, sapply(dadaFs, getN), sapply(mergers, getN), rowSums(seqtab), rowSums(seqtab.nochim))
    colnames(track) <- c("input", "filtered", "denoised", "merged", "tabled", "nonchim")
    rownames(track) <- sample.names
    head(track)
    taxa <- assignTaxonomy(seqtab.nochim, "Training/rdp_train_set_16.fa.gz", multithread=TRUE)
    unname(head(taxa))
    """
}

result.subscribe {
    println it.trim()
}
