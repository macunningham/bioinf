#!/usr/bin/env nextflow

params.input = "$baseDir/MiSeq_SOP/*.fastq"
fqs = Channel.fromPath(params.input)

params.outDir="$baseDir"

process R_hello {

    publishDir params.outDir, mode: 'copy'

    output:
    file "my.txt" into testChannel
    stdout result

    input:
    file fq from fqs.first()

    """

    #!/usr/bin/env Rscript
    print(getwd())
    file.create("my.txt")
    write("addthis",file="my.txt",append=TRUE)
    system("cat my.txt")
    write("addthistoo",file="my.txt",append=TRUE)
    system("cat my.txt")
    print("${fq.baseName}")

    """
}

result.subscribe {
    println it.trim()
}
