#!/bin/bash
# Note: file paths refer to KD's documents in the AIMS HPC

# Validate the mapping file:

validate_mapping_file.py \
-m /export/home/f-k/kdamjano/Analysis_2016/Mapping_files/Mapping_File.txt \
-o /export/home/f-k/kdamjano/Analysis_2016/Mapping_files \
-b -p


#Trimmomatic (conservative trimming)
# -trim 5 and 3' ends until q >= 3
# -trim from 3' end until [qqqq] > 20
# -discard sequences < 170 bases
# Output is: forward paired / forward unpaired / reverse paired / reverse unpaired

for i in {100..300} # Numbers depend on the "S" names of our files (Ramaciotti output)

do

if [ -f /export/home/f-k/kdamjano/Sequences_submit/*_S${i}_L001_R1_001.fastq.gz ]

then

java -jar /export/home/f-k/kdamjano/Trimmomatic-0.36/trimmomatic-0.36.jar \
PE -threads 16 -phred33 \
-trimlog /export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/${i}_trimlog.txt \
/export/home/f-k/kdamjano/Sequences_submit/*_S${i}_L001_R1_001.fastq.gz \
/export/home/f-k/kdamjano/Sequences_submit/*_S${i}_L001_R2_001.fastq.gz \
/export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/FP1_${i}.gz \
/export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/fu1_${i}.gz \
/export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/RP2_${i}.gz \
/export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/ru2_${i}.gz \
LEADING:3 TRAILING:3 SLIDINGWINDOW:4:20 MINLEN:170

fi
done


# Pair-end joining with PEAR:
# Better than the QIIME method (multiple_join_paired_ends.py) because it has a low false positive rate.
# The first three lines are the forward read, reverse read, and output, respectively.
# The last line specifies the minimum assembly length (150bp).

module load pear

for i in {100..300}

do

if [ -f /export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/FP1_${i}.gz ]

then

pear \
-f /export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/FP1_${i}.gz \
-r /export/home/f-k/kdamjano/Analysis_2016/TrimmedSub/RP2_${i}.gz \
-o /export/home/f-k/kdamjano/Analysis_2016/PairedSub/${i}.fasta \
-n 150

fi

done


# Convert .fastq files into .fasta format:

for i in {100..300}

do

if [ -f /export/home/f-k/kdamjano/Analysis_2016/PairedSub/${i}.fasta.assembled.fastq ]

then

convert_fastaqual_fastq.py \
-f /export/home/f-k/kdamjano/Analysis_2016/PairedSub/${i}.fasta.assembled.fastq \
-o /export/home/f-k/kdamjano/Analysis_2016/PairedSub_fasta \
-c fastq_to_fastaqual \

fi

done


# Check for FASTA errors:
# Call the following function in he working directory:
# The first line is input, the second line is output. No -i or -o necessary

for i in {100..300}

do

if [ -f /export/home/f-k/kdamjano/Analysis_2016/PairedSub_fasta/${i}.fasta.assembled.fna ]

then

python parse_fasta_errors.py \
/export/home/f-k/kdamjano/Analysis_2016/PairedSub_fasta/${i}.fasta.assembled.fna \
/export/home/f-k/kdamjano/Analysis_2016/PairedSub_checked/${i}.fasta

fi

done


#Add QIIME labels
# -add the appropriate sample name to each sequence header
# -add a unique number identifier to each sequence header
# -combine all sequences into a single fasta file

add_qiime_labels.py \
--mapping_fp /export/home/f-k/kdamjano/Analysis_2016/Mapping_files/Mapping_File_corrected_submit.txt \
--fasta_dir /export/home/f-k/kdamjano/Analysis_2016/PairedSub_checked \
--filename_column InputFilename \
--count_start 1000000 \
--output_dir /export/home/f-k/kdamjano/Analysis_2016/CombinedSub


#Count the total combined sequences

count_seqs.py \
--input_fp /export/home/f-k/kdamjano/Analysis_2016/Combined/combined_seqs.fna \
--output_fp /export/home/f-k/kdamjano/Analysis_2016/Combined/combined_seqs_count.txt

#Chimera check
# -compare combined sequences to abridged chimera-free database: http://drive5.com/uchime/rdp_gold.fa
# -generate list of suspected chimeric sequences
#######################################################################################################
identify_chimeric_seqs.py \
--chimera_detection_method usearch61 \
--input_fasta_fp /export/home/f-k/kdamjano/Analysis_2016/CombinedSub/combined_seqs.fna \
--reference_seqs_fp /export/home/f-k/kdamjano/Analysis_2016/Databases/rdp_gold.fa \
--output_fp /export/home/f-k/kdamjano/Analysis_2016/ChimerasSub

#Delete chimeric sequences from combined sequence file

filter_fasta.py \
--input_fasta_fp /export/home/f-k/kdamjano/Analysis_2016/CombinedSub/combined_seqs.fna \
--seq_id_fp /export/home/f-k/kdamjano/Analysis_2016/ChimerasSub/chimeras.txt \
--negate \
--output_fasta_fp /export/home/f-k/kdamjano/Analysis_2016/CombinedSub/non_chimeric_seqs.fna

#Count total remaining sequences

count_seqs.py \
--input_fp /export/home/f-k/kdamjano/Analysis_2016/Combined/non_chimeric_seqs.fna \
--output_fp /export/home/f-k/kdamjano/Analysis_2016/Combined/non_chimeric_seqs_count.txt



#Pick OTUs using QIIME's open OTU picking method
# -parameter file sets non_default values for workflow steps
# -the output directory is created by QIIME
# Note: we use the 119 release of SILVA, and specify 97% identity

pick_open_reference_otus.py \
--otu_picking_method uclust \
--input_fps /export/home/f-k/kdamjano/Analysis_2016/Combined/non_chimeric_seqs.fna \
--reference_fp /export/databases/SILVA/Silva119_release/rep_set/97/Silva_119_rep_set97.fna \
--force --parallel --jobs_to_start 12 \
--parameter_fp /export/home/f-k/kdamjano/Analysis_2016/Scripts/Parameters_upstream_2016.txt \
--output_dir /export/home/f-k/kdamjano/Analysis_2016/OTUs


# Filter the OTU table to remove samples with zero counts 

filter_samples_from_otu_table.py \
--input_fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_mc2_w_tax_no_pynast_failures.biom \
--min_count 1 \
--output_fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_mc2_w_tax_no_pynast_failures_NoZeros.biom

#Remove OTUS derived from sequences that comprise <0.005% of all sequences
#There is a high probability that such rare sequences are spurious

filter_otus_from_otu_table.py \
--input_fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_mc2_w_tax_no_pynast_failures_NoZeros.biom \
--min_count_fraction 0.00005 \
--output_fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_filtered.biom

#Filter chloroplasts from OTU table if they are present
# -removes sequences derived from plants, such as algae

filter_taxa_from_otu_table.py \
--input_otu_table_fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_filtered.biom \
--negative_taxa D_2__Chloroplast \
--output_otu_table /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_final.biom

#Produce report of final OTU table

biom summarize_table \
--input-fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_final.biom \
--output-fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_summary.txt


#Make taxonomic summary charts

summarize_taxa_through_plots.py \
--otu_table_fp /export/home/f-k/kdamjano/Analysis_2016/OTUs/otu_table_final.biom \
--mapping_fp /export/home/f-k/kdamjano/Analysis_2016/Mapping_files/Mapping_File_corrected.txt \
--mapping_category Category \
--sort --force \
--output_dir /export/home/f-k/kdamjano/Analysis_2016/Taxa_plots


#Provides the number of OTUs per sample:

biom summarize-table \
-i /export/home/f-k/kdamjano/Analysis_2016/Taxa_plots/Category_otu_table_sorted_L6.biom \
--qualitative \
-o /export/home/f-k/kdamjano/Analysis_2016/OTUs/genus_per_sample.txt








