#!/bin/bash

rm -r /mnt/leon_data/v4/seqs/trimmed/
rm -r /mnt/leon_data/v4/seqs/paired/
rm -r /mnt/leon_data/v4/seqs/combined/
rm -r /mnt/leon_data/v4/seqs/usearch61_chimeras/
rm -r /mnt/leon_data/v4/v4_otus/
mkdir /mnt/leon_data/v4/seqs/trimmed
mkdir /mnt/leon_data/v4/seqs/paired
mkdir /mnt/leon_data/v4/seqs/combined
mkdir /mnt/leon_data/v4/seqs/usearch61_chimeras

#Trimmomatic (conservative trimming)
# -trim 5 and 3' ends until q >= 3
# -trim from 3' end until [qqqq] > 15
# -discard sequences < 170 bases

for i in {195..255}

do

if [ -f /mnt/leon_data/v4/seqs/raw/*_S${i}_L001_R1_001.fastq.gz ]

then

java -jar /opt/Trimmomatic/trimmomatic-0.36.jar \
PE -threads 16 -phred33 \
-trimlog /mnt/leon_data/v4/seqs/trimmed/${i}_trimlog.txt \
/mnt/leon_data/v4/seqs/raw/*_S${i}_L001_R1_001.fastq.gz \
/mnt/leon_data/v4/seqs/raw/*_S${i}_L001_R2_001.fastq.gz \
/mnt/leon_data/v4/seqs/trimmed/FT1_${i}.gz \
/mnt/leon_data/v4/seqs/trimmed/fu1_${i}.gz \
/mnt/leon_data/v4/seqs/trimmed/RT2_${i}.gz \
/mnt/leon_data/v4/seqs/trimmed/ru2_${i}.gz \
LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:170

#PandaSeq
# -use the PEAR algorithm to join pairs because low false +ves
# -remove primers
# -discard sequences < 150 bases
# -output files in fasta format

pandaseq \
-f /mnt/leon_data/v4/seqs/trimmed/FT1_${i}.gz \
-r /mnt/leon_data/v4/seqs/trimmed/RT2_${i}.gz \
-A pear \
-p TATGGTAATTGTGTGYCAGCMGCCGCGGTAA \
-q AGTCAGTCAGCCGGACTACNVGGGTWTCTAAT \
-l 250 \
-w /mnt/leon_data/v4/seqs/paired/${i}.fasta \
-g /mnt/leon_data/v4/seqs/paired/${i}_pandalog.txt

fi

done

echo "Seqs trimmed..."
echo "Seqs paired..."

#Add QIIME labels
# -add the appropriate sample name to each sequence header
# -add a unique number identifier to each sequence header
# -combine all sequences into a single fasta file
add_qiime_labels.py \
--mapping_fp /mnt/leon_data/v4/leon_v4_map.txt \
--fasta_dir /mnt/leon_data/v4/seqs/paired \
--filename_column InputFileName \
--count_start 1000000 \
--output_dir /mnt/leon_data/v4/seqs/combined

echo "QIIME labels added..."
echo "Seqs combined..."

#Count the total combined sequences
count_seqs.py \
--input_fp /mnt/leon_data/v4/seqs/combined/combined_seqs.fna \
--output_fp /mnt/leon_data/v4/seqs/combined/combined_seqs_count.txt

echo "Combined seqs counted..."

#Chimera check
# -compare combined sequences to abridged chimera-free database
# -generate list of suspected chimeric sequences
identify_chimeric_seqs.py \
--chimera_detection_method usearch61 \
--input_fasta_fp /mnt/leon_data/v4/seqs/combined/combined_seqs.fna \
--reference_seqs_fp /opt/chimera_dbs/rdp_gold.fa \
--output_fp /mnt/leon_data/v4/seqs/usearch61_chimeras

echo "Chimeras checked..."

#Delete chimeric sequences from combined sequence file
filter_fasta.py \
--input_fasta_fp /mnt/leon_data/v4/seqs/combined/combined_seqs.fna \
--seq_id_fp /mnt/leon_data/v4/seqs/usearch61_chimeras/chimeras.txt \
--negate \
--output_fasta_fp /mnt/leon_data/v4/seqs/usearch61_chimeras/non_chimeric_seqs.fna

echo "Chimeras deleted..."

#Count total remaining sequences
count_seqs.py \
--input_fp /mnt/leon_data/v4/seqs/usearch61_chimeras/non_chimeric_seqs.fna \
--output_fp /mnt/leon_data/v4/seqs/usearch61_chimeras/non_chimeric_seqs_count.txt

echo "Surviving seqs counted..."

#Pick OTUs using QIIME's open OTU picking method
# -the parameter file sets non default values for workflow scripts
# -the output directory is created by QIIME
#  IT MUST NOT ALREADY EXIST (unless the --force option is passed)!
pick_open_reference_otus.py \
--otu_picking_method uclust \
--force --parallel --jobs_to_start 8 \
--input_fps /mnt/leon_data/v4/seqs/usearch61_chimeras/non_chimeric_seqs.fna \
--reference_fp /mnt/Silva123/rep_set/rep_set_16S_only/97/97_otus_16S.fasta \
--parameter_fp /mnt/leon_data/v4/v4_parameters_upstream_123.txt \
--output_dir /mnt/leon_data/v4/v4_otus

echo "OTUs picked..."
