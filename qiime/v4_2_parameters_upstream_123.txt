# Pick reference OTUs parameters
# Reference sequences for the closed reference OTU picking step.

pick_otus.py:refseqs_fp	/mnt/Silva123/rep_set/rep_set_16S_only/97/97_otus_16S.fasta


# Multiple sequence alignment parameters
# The min_percent_id recommended for QIIME analyses by SILVA is 60%

align_seqs:template_fp	/mnt/Silva123/core_alignment/core_alignment_SILVA123.fasta
align_seqs:min_percent_id	60.0


# Filter alignment parameters
# These parameters overide the use of the Lane mask, which is the default behaviour.
# A Lane mask file is not provided with SILVA. Instead, the following parameters are recommended by the SILVA developers.

filter_alignment:allowed_gap_frac	0.80
filter_alignment:entropy_threshold	0.10
filter_alignment:suppress_lane_mask_filter	True


# Taxonomy assignment parameters

assign_taxonomy:id_to_taxonomy_fp	/mnt/Silva123/taxonomy/16S_only/97/taxonomy_7_levels.txt
assign_taxonomy:reference_seqs_fp	/mnt/Silva123/rep_set/rep_set_16S_only/97/97_otus_16S.fasta
