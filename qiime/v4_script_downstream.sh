#!/bin/bash

rm /mnt/leon_data/v4/v4_otus/otu_table_filtered.biom
rm /mnt/leon_data/v4/v4_otus/otu_table_final.biom
rm /mnt/leon_data/v4/v4_otus/otu_table_seq_counts.txt
rm /mnt/leon_data/v4/v4_otus/otu_table_otu_counts.txt
rm -r /mnt/leon_data/v4/v4_taxa_summary
rm -r /mnt/leon_data/v4/alpha_rare
mkdir /mnt/leon_data/v4/alpha_rare

#Remove OTUS derived from sequences that comprise <0.005% of all sequences

filter_otus_from_otu_table.py \
--input_fp /mnt/leon_data/v4/v4_otus/otu_table_mc2_w_tax_no_pynast_failures.biom \
--min_count_fraction 0.00005 \
--output_fp /mnt/leon_data/v4/v4_otus/otu_table_filtered.biom

echo "OTUs with low rel abundance seqs filtered..."

#Remove any eukaryotes and chloroplasts from OTU table

filter_taxa_from_otu_table.py \
--input_otu_table_fp /mnt/leon_data/v4/v4_otus/otu_table_filtered.biom \
--negative_taxa D_0__Eukaryota,D_2__Chloroplast \
--output_otu_table /mnt/leon_data/v4/v4_otus/otu_table_final.biom

echo "Eukaryotes and Chloroplasts filtered..."

#Produce reports of final OTU table

biom summarize-table \
--input-fp /mnt/leon_data/v4/v4_otus/otu_table_final.biom \
--output-fp /mnt/leon_data/v4/v4_otus/otu_table_seq_counts.txt

biom summarize-table --qualitative \
--input-fp /mnt/leon_data/v4/v4_otus/otu_table_final.biom \
--output-fp /mnt/leon_data/v4/v4_otus/otu_table_otu_counts.txt

echo "BIOM table summarised..."

#Make taxonomic summary charts

summarize_taxa_through_plots.py \
--otu_table_fp /mnt/leon_data/v4/v4_otus/otu_table_final.biom \
--mapping_fp /mnt/leon_data/v4/leon_v4_map.txt \
--mapping_category Culture \
--parameter_fp /mnt/leon_data/v4/v4_parameters_downstream_123.txt \
--sort --force \
--output_dir /mnt/leon_data/v4/v4_taxa_summary

echo "Taxa summary charts created..."

#Normalise seqs/sample by rarefying
#If any samples disappear, then depth was too shallow

single_rarefaction.py \
--input_path /mnt/leon_data/v4/v4_otus/otu_table_final.biom \
--depth 22000 \
--output_path /mnt/leon_data/v4/alpha_rare/otu_table_final_rare.biom

echo "Sample data normalised by rarefying..."

#Generate alpha diversity indices

parallel_alpha_diversity.py \
--input_path /mnt/leon_data/v4/alpha_rare/ \
--output_path /mnt/leon_data/v4/alpha_rare/ \
--tree_path /mnt/leon_data/v4/v4_otus/rep_set.tre \
--metrics shannon,simpson \
--jobs_to_start 8

echo "Alpha diversity metrics calculated from rarefied BIOM table..."
echo "Done!"
