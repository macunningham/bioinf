This document contains instructions for starting an instance (a virtual machine = cloud computer) on Nectar.

Click on the following link for instructions

Link below takes you to 
https://teams.microsoft.com/l/file/E6D485F6-9DC9-4B00-9423-962C7D528DA9?tenantId=0e5bf3cf-1ff4-46b7-9176-52c538c22a4d&fileType=docx&objectUrl=https%3A%2F%2Funimelbcloud.sharepoint.com%2Fteams%2FEMRI%2FShared%20Documents%2FEvents%2FHow%20To_Nectar-NGS_3.4_workshop.docx&baseUrl=https%3A%2F%2Funimelbcloud.sharepoint.com%2Fteams%2FEMRI&serviceName=teams&threadId=19:e209704799994c359ab19adf63958188@thread.skype&groupId=10b2f931-9810-4b14-965e-775961dd4de6

Notes for easy implimentation
When selecting availability zone, use "intersect"
"Spawning" means hit "Launch" and wait 2-3 min
