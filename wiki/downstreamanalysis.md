With Docker installed - if not installed, go to [docker install instructions](dockermacinstall.md)

* At the command line, type 'docker pull rocker/tidyverse'. [See here for more information](https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image)
* to check the image was downloaded, type 'docker images'
* type 'sudo docker run -d -p 8787:8787 rocker/tidyverse'; put in computer password.
* type 'docker ps' - for a list of runnning containers
* open new browser tab, paste in 'localhost:8787', username and password are both 'rstudio'
* to remove container (= instance of docker image), type 'docker stop container id' (e.g. 'docker stop 84cdf71f38de'), then 'docker rm container id' (e.g. 'docker rm 84cdf71f38de')
