
Welcome to the lab wiki. Please use it to share information you think might be useful for other lab / project members.

### spaces

[know how](knowhow.md) use this space to learn / share info about methods. for example (a) printing documents (b) feeding the anemones or (c) running a bioinformatic analysis.

[ideas] - use this space to learn / share info about anything else including ideas, new research.

### how to contribute 

This wiki is in markdown format. See markdown cheatsheet here..
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
