### general
* Send a letter
 
### wet lab
* [[fully describe processes in amplicon library sequencing by illumina]]
* [metabarcoding](metabarcoding.md)

### bioinformatics
* [[run a qiime analysis]]
* [[resources]]
* [run dada2](rundada2.md)
* [ncbi sra download](ncbi_sra_download.md)
