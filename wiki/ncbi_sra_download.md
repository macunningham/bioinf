* prereqs
  * docker is installed
* background
  * https://edwards.sdsu.edu/research/fastq-dump/
* steps 
  * run cmd 'docker pull https://hub.docker.com/r/inutano/sra-toolkit/'
  * for full download, run cmd 'docker run --rm -v "$(pwd)":/data -w /data inutano/sra-toolkit fastq-dump --outdir fastq --gzip --skip-technical  --readids --read-filter pass --dumpbase --split-files --clip {srr data set id}' 
    * replace {srr data set id} with the SRA data set you wish to download e.g. 'SRR390728' 
  * to get subsample of reads, run cmd 'docker run --rm -v "$(pwd)":/data -w /data inutano/sra-toolkit fastq-dump -N 10000 -X 10005 --outdir fastq --gzip --skip-technical  --readids --read-filter pass --dumpbase --split-files --clip {srr data set id}'
