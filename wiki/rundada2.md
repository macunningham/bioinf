This is a step-by-step how-to for running the amplicon sequence data analysis software dada2 on the Nectar cloud
### Steps 
* get sequence data from ?
* do dada2 (https://benjjneb.github.io/dada2/tutorial.html) analysis in Nectar
  * launch virtual machine in Nectar
    * Open http://nectar.org.au/
    * Click 'cloud login' at the top right of the page
    * (optional) Select 'University of Melbourne' as your institution if prompted
    * Enter your username and password and hit return
    * Click 'Orchestration' in the navigation menu at left
    * Click 'Stacks'
    * Select 'Resume Stack' in the 'Check Stack' drop down box on the right
    * Click 'Compute' in the navigation menu at left
    * Click 'Instances'
    * Copy IP address from 'instance name' (e.g. xxlarge) - there might two IP addresses and you might need to try them both! In this case '203.101.226.33' works.
  * go to command line (e.g. 'terminal') to pass commands to the Nectar VM
    * run 'cd ~/.ssh' (tilda takes you to home directory where '.ssh' is located)
    * run 'ls' -> 'Nectar_Key	Nectar_Key.pub	known_hosts' - these files were copied into .ssh previously
    * run 'ssh -i Nectar_Key ubuntu@203.101.226.33'
    * 'Enter passphrase for key 'Nectar_Key'' - in this case 'mitch'. This step is now completed. You are now in Nectar - see the prompt to confirm 'ubuntu@xxlarge-basic-server-5qn7a7z4ryoh:'
  * run dada2 analysis
    * 'ls' to orientate -> 'nf' -> 'cd nf'
    * to look in nf files, type 'nano filename' (e.g. nano mainLinda.nf). 'bash' - no functionality - it passes stuff to other programs - e.g. 'ls' (program) = list files in the directory. 'nano' - (https://en.wikipedia.org/wiki/GNU_nano)
    * type 'sudo nextflow mainLinda.nf -with-docker mitchac/dada2'. nextflow - (https://www.nextflow.io/docs/latest/basic.html)
    * when running:
      * 'N E X T F L O W  ~  version 0.25.4
Launching `mainLinda.nf` [tender_joliot] - revision: 650d6e61a2
[warm up] executor > local
[59/b0f03a] Submitted process > Dada2 (1)'
    * nextflow is running the pipeline ('mainLinda.nf')
   * retrieve output files
      * TBC pull off with ftp client 
   * suspend Nectar VM
      * ..... Linda to complete: how to do this.. Orchestration, Stacks, pull down menu...
      * blah blah 

